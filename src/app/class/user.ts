export class User{

    public key: string = ""

    constructor(
        public name: string = "",
        public surname: string = "",
        public birthday: string = "",
        public birthplace: string = "",
        public province_birthplace: string = "",
        public livingcity: string = "",
        public province_livingcity: string = "",
        public address1: string = "",
        public address2: string = "",
        public phone: string = "",
        public fiscalcode: string = "",
        public email: string = "",
        public passport: string = "",
        public password: string = ""
    ){}

}