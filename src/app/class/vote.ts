export class Vote {

    public id: number = -1
    
    
    constructor(public date: Date,
                public type: string,
                public name: string,
                public is_old: boolean = false,
                public is_future: boolean = false,
                public is_actual: boolean = false,
                public voted: boolean = false,
                public data: any = {},
                public choises: Object = null,
                public results: Array<Array<any>> = [],
                public key: string = null) {
                    let datakeys = Object.keys(data)
                    if(datakeys.includes('choises') && datakeys.includes('results')){
                        var ch = this.data['choises']
                        if (!Array.isArray(ch)) {
                            ch = Object.keys(ch)
                        }
                        this.results = ch.map((c, i) => [c, this.data['results'][i]])
                    }
                }
                
}
