import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Vote } from 'src/app/class/vote';
import { Location } from '@angular/common';

@Component({
  selector: 'app-candidatesselection',
  templateUrl: './candidatesselection.page.html',
  styleUrls: ['./candidatesselection.page.scss'],
})
export class CandidatesselectionPage implements OnInit {
  public vote: Vote
  public party: string = ""
  public chamber: string = ""
  choises: string[]
  private selected: string[] = []
  tot_selectables = -1
  remaining = -1
  constructor(private router: Router,
    private loc: Location) {
    let nav = this.router.getCurrentNavigation()
    console.log(nav.extras.state)
    this.vote = nav.extras.state['vote']
    this.party = nav.extras.state['party']
    this.chamber = nav.extras.state['chamber']
    this.tot_selectables = this.vote.data['toselect']
    this.remaining = this.vote.data['toselect']
    this.choises = this.vote.data['choises'][this.party]
  }

  on_change(event, c: string) {
    if (event.detail.checked) {
      this.on_select_candidate(c)
    } else {
      this.on_deselect_candidate(c)
    }
  }

  on_change_empty_vote(event) {
    if (event.detail.checked) {
      this.remaining -= this.tot_selectables
    } else {
      this.remaining += this.tot_selectables
    }
  }

  on_select_candidate(candidate: string) {
    this.remaining -= 1
    this.selected.push(candidate)
  }

  on_deselect_candidate(candidate: string) {
    this.remaining += 1
    this.selected = this.selected.filter(v => v != candidate)
  }

  is_chosen(c: string): boolean {
    return this.selected.includes(c)
  }

  empty_selection(): boolean {
    return this.selected.length == 0
  }

  confirm_vote() {
    this.vote.data['selected_party_' + this.chamber] = this.party
    this.vote.data['selected_candidates_' + this.chamber] = this.selected
    this.vote.voted = true
    let senate_voted = Object.keys(this.vote.data).includes("selected_party_senate")
    let deputies_voted = Object.keys(this.vote.data).includes("selected_party_deputies")
    if (senate_voted && deputies_voted) {
      this.router.navigateByUrl('/homepage')
    } else {
      if (deputies_voted) {
        this.router.navigateByUrl('/chamberselection/senate', {state: {vote: this.vote, chamber: 'senate'}})
      } else {
        this.router.navigateByUrl('/chamberselection/deputies', {state: {vote: this.vote, chamber: 'deputies'}})
      }
    }
  }

  goback() {
    this.router.navigateByUrl('/chamberselection/' + this.chamber, {state: {vote: this.vote, chamber: this.chamber}, skipLocationChange: true})
  }

  public wiki_url(c: string) {
    let query_param = c.replace("  ", " ").replace(" ", "+")
    let url = "https://it.wikipedia.org/wiki/Special:Search?search=" + query_param + "&go=Go&ns0=1"
    window.open(url)
  }


  ngOnInit() {
  }

}
