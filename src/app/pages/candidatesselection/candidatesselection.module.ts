import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CandidatesselectionPage } from './candidatesselection.page';

const routes: Routes = [
  {
    path: 'senate/:i',
    component: CandidatesselectionPage
  },
  {
    path: 'deputies/:i',
    component: CandidatesselectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CandidatesselectionPage]
})
export class CandidatesselectionPageModule {}
