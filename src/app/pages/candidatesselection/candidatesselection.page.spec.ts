import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatesselectionPage } from './candidatesselection.page';

describe('CandidatesselectionPage', () => {
  let component: CandidatesselectionPage;
  let fixture: ComponentFixture<CandidatesselectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatesselectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesselectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
