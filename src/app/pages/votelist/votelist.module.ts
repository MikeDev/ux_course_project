import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ActivatedRoute, Router } from '@angular/router';
import { Pipe, PipeTransform } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { IonicModule } from '@ionic/angular';
import { VoteProviderService } from 'src/app/service/vote-provider.service';
import { VotelistPage } from './votelist.page';

const routes: Routes = [
  {
    path: '',
    component: VotelistPage
  }
];


@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter( it => {
          return it.name.toLowerCase().includes(searchText);
        });
   }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VotelistPage, FilterPipe],
  providers: [
    LocalNotifications,
    VoteProviderService
  ]
})
export class VotelistPageModule {

  constructor(){
  }

  
}
