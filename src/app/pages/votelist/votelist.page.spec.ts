import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotelistPage } from './votelist.page';

describe('VotelistPage', () => {
  let component: VotelistPage;
  let fixture: ComponentFixture<VotelistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotelistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotelistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
