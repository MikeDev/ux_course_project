import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Route } from '@angular/router';
import { Vote } from '../../class/vote'
import { Observable } from 'rxjs';
import { NavParams, NavController } from '@ionic/angular';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { Platform, AlertController } from '@ionic/angular';
import { delay } from 'q';
import { VoteProviderService } from 'src/app/service/vote-provider.service';


@Component({
  selector: 'app-votelist',
  templateUrl: './votelist.page.html',
  styleUrls: ['./votelist.page.scss'],
})
export class VotelistPage implements OnInit {
  private mode: string = undefined
  public filter_value: string = ""
  public votes: Array<Vote> = []
  public show_notification = true
  private votes_obs: Observable<Vote>

  constructor(private route: ActivatedRoute,
    private router: Router,
    private not: LocalNotifications,
    private alertCtrl: AlertController,
    private vote_service: VoteProviderService) {
      this.get_votes()
    // this.not.on('trigger').subscribe(res => {
    //   let msg = res.data ? res.data.mydata : '';
    //   this.show_alert(res.title, res.text, msg);
    // });
  }


  show_alert(header, sub, msg) {
    this.alertCtrl.create({
      header: header,
      subHeader: sub,
      message: msg,
    }).then(alert => alert.present());
  }
  get_votes() {
    let filter_attribute = 'is_' + this.mode
    let votes = this.vote_service.pull_votes().map((vs) => vs.filter((v => v[filter_attribute])))
    if(filter_attribute == 'is_active'){
      votes.subscribe((votes) => {
        if (this.show_notification && votes.filter((v) => v.is_actual && !v.voted).length > 0) {
          this.not.schedule(
            {
              id: 0,
              title: 'Remainder',
              text: "You have some votes to do",
              trigger: { in: 2, unit: ELocalNotificationTriggerUnit.SECOND },
              sound: null,
            }
          )
          this.show_notification = false
          let p = new Promise(() => { this.show_notification = true })
          delay(p, 1000 * 60 * 10)
        }
      })
    }
    votes.subscribe((votes) => this.votes = this.votes.concat(votes))
  }

  async get_mode() {
    let params = await this.route.queryParams.toPromise()
    console.log(params)
    if (Object.keys(params).includes('mode')) {
      this.mode = params['mode']
    } else {
      this.mode = 'active'
    }

  }

  public filte_vote(vote: Vote) {
    return vote.name.includes(this.filter_value)
  }

  goto_details(vote: Vote) {
    if (vote.is_actual && !vote.voted) {
      this.router.navigateByUrl('/votedisclamer', { state: { vote: vote } })
    }
    else if (vote.is_actual && vote.voted) {
      this.router.navigateByUrl('/chamberselection/deputies', { state: { vote: vote, chamber: 'deputies'} })
    }
    else if (vote.is_old) {
      this.router.navigateByUrl('/voteresults', { state: { vote: vote } })
    }
  }

  ngOnInit() {
    this.mode = this.route.snapshot.data['mode'];
    this.get_votes()
  }

}
