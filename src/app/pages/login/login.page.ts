import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { UserManagerService } from 'src/app/service/user-manager.service';
import { User } from '../../class/user'
import { File } from '@ionic-native/file/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private users: User[] = []
  public user: string = ""
  public pass: string = ""
  public error_msg = ""
  public has_fingerprint = false
  public login_file_exists = false
  public pass_disabled = false

  constructor(private router: Router,
    private um: UserManagerService,
    private file: File) {
  }

  ngOnInit() {
  }

  public goto_registration() {
    this.router.navigateByUrl('/registration')
  }

  private find_user() {
    for (let u of this.users) {
      if (this.user == u.passport && this.pass == u.password) {
        return u
      }
    }
    return null
  }

  private login(user) {
    this.router.navigateByUrl('/homepage', { state: { user: user } })

  }

  public check_login() {
    this.error_msg = ''
    let user = this.find_user()
    if (user != null) {
      if (this.has_fingerprint) {
        this.file.checkFile(this.file.dataDirectory, "login_vote_app")
          .then(() => { this.login(user) })
          .catch(() => {
            this.file.writeFile(this.file.dataDirectory, 'login_vote_app', user.passport + ' ' + user.password)
              .then(() => this.login(user)).catch(() => this.login(user))
          })
      } else {
        this.login(user)
      }
    } else {
      this.error_msg = "Passport ID or password wrong"
    }
  }

  fingerprint_login() {
    this.file.checkFile(this.file.dataDirectory, 'login_vote_app').then(() => {
      this.login_file_exists = true
      if (this.has_fingerprint) {
        console.log('show');
        FingerprintAIO.show({
          clientId: "Fingerprint-Demo",
          clientSecret: 'password',
          disableBackup: true
        }).then(result => {
          this.file.readAsText(this.file.dataDirectory, 'login_vote_app').then(
            (text) => {
              let userpass = text.split(' ', 2)
              this.user = userpass[0]
              this.pass = userpass[1]
              this.check_login()
            }
          )
        }
        ).catch(err => {

        });
      }
    }).catch(() => this.check_login())
  }

  redirect_on_first_opening() {
    this.file.checkFile(this.file.dataDirectory, 'first_login').then(() => {

    }).catch(() => {
      this.file.writeFile(this.file.dataDirectory, 'first_login', "").then(() => this.goto_registration())
    })
  }

  ionViewDidEnter() {
    try{
      this.redirect_on_first_opening()
    } catch(e){}
    this.users = []
    this.um.pull_users().subscribe((users_) => {
      users_.forEach((u) => this.users.push(u))
    })
    FingerprintAIO.isAvailable().then(() => {
      this.has_fingerprint = true
      this.fingerprint_login()
    }).catch(() =>
      this.has_fingerprint = false
    )
  }

}
