import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChamberselectionPage } from './chamberselection.page';

const routes: Routes = [
  {
    path: '',
    component: ChamberselectionPage,
    children: [
      {
        path: 'deputies',
        data: {'chamber': 'deputies'},
        loadChildren: '../voteselection/voteselection.module#VoteselectionPageModule'
      },
      {
        path: 'senate',
        data: {'chamber': 'senate'},
        loadChildren: '../voteselection/voteselection.module#VoteselectionPageModule'
      }

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChamberselectionPage]
})
export class ChamberselectionPageModule {}
