import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Vote } from 'src/app/class/vote';

@Component({
  selector: 'app-chamberselection',
  templateUrl: './chamberselection.page.html',
  styleUrls: ['./chamberselection.page.scss'],
})
export class ChamberselectionPage implements OnInit {
  private vote: Vote
  constructor(private router: Router) {
    let state = this.router.getCurrentNavigation().extras.state
      if(state !== undefined && state !== null){
        this.vote = state['vote']
      }
  }


  goto_tab(chamber: string) {
    this.router.navigateByUrl('/chamberselection/' + chamber, { state: { vote: this.vote } })
  }

  ngOnInit() {
  }

}
