import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChamberselectionPage } from './chamberselection.page';

describe('ChamberselectionPage', () => {
  let component: ChamberselectionPage;
  let fixture: ComponentFixture<ChamberselectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChamberselectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChamberselectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
