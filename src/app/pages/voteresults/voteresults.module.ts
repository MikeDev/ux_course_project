import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { GoogleChartsModule } from 'angular-google-charts';
import { VoteresultsPage } from './voteresults.page';

const routes: Routes = [
  {
    path: '',
    component: VoteresultsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoogleChartsModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [VoteresultsPage]
})
export class VoteresultsPageModule {}
