import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteresultsPage } from './voteresults.page';

describe('VoteresultsPage', () => {
  let component: VoteresultsPage;
  let fixture: ComponentFixture<VoteresultsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoteresultsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteresultsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
