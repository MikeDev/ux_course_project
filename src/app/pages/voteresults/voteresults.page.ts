import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as CanvasJS from '../../canvasjs.min';
import { Location } from '@angular/common';
import { Vote } from 'src/app/class/vote';
@Component({
  selector: 'app-voteresults',
  templateUrl: './voteresults.page.html',
  styleUrls: ['./voteresults.page.scss'],
})
export class VoteresultsPage implements OnInit {
  private labels: string[] = ["A", "B", "C"]
  private vote_values = [29, 58, 13]
  public data = []
  public type1 = "PieChart"
  public type2 = "BarChart"
  private chart = null
  public vote: Vote
  constructor(private route: ActivatedRoute, public loc: Location, private router: Router) {
    this.vote = this.router.getCurrentNavigation().extras.state['vote']
    this.data = this.vote.results
    console.log(this.vote)
  }

  ngOnInit() {
    // this.data = this.labels.map((el, i) => [el, this.vote_values[i]])
  }
}
