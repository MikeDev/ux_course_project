import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewController } from '@ionic/core';
import { NavParams } from '@ionic/angular';
import { UserManagerService } from 'src/app/service/user-manager.service';

@Component({
  selector: 'app-popover-menu',
  templateUrl: './popover-menu.component.html',
  styleUrls: ['./popover-menu.component.scss'],
})
export class PopoverMenuComponent implements OnInit {
  public popover: any
  private user
  constructor(public navParams:NavParams, private router: Router, private user_service: UserManagerService) {
    this.user = navParams.get('user')
   }

  ngOnInit() {}

  public goto_electoralcard() {
    this.router.navigateByUrl('/electoral-card', {state: {elector: this.user}})
    this.popover.dismiss()
  }
  
  public logout(){
    this.user_service.eliminate_current()
    this.router.navigateByUrl('/login')
    this.popover.dismiss()
  }

}
