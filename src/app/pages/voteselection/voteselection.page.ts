import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Vote } from 'src/app/class/vote';

@Component({
  selector: 'app-voteselection',
  templateUrl: './voteselection.page.html',
  styleUrls: ['./voteselection.page.scss'],
})
export class VoteselectionPage implements OnInit {
  public vote: Vote
  private chamber: string = ""
  public choises: Array<string> = []

  constructor(private route: ActivatedRoute, private router: Router) {
    this.vote = this.router.getCurrentNavigation().extras.state['vote']
    this.choises = Object.keys(this.vote.data['choises'])
    console.log(this.choises)
  }

  select_candidate(i: Number, party: string){
    this.router.navigateByUrl('/candidatesselection/' + this.chamber + '/' + i, {state: {party: party, vote: this.vote, chamber: this.chamber}, skipLocationChange: true})
  }
  
  goto_emptyvote(){
    this.router.navigateByUrl('/candidatesselection/' + this.chamber + '/-1',
                               {state: {party: '', vote: {data: {toselect: 0, choises: {}}}, chamber: this.chamber }, skipLocationChange: true})
  }


  ngOnInit() {
    this.chamber = this.route.snapshot.data['chamber'];
  }

}
