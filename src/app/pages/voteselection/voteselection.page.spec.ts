import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteselectionPage } from './voteselection.page';

describe('VoteselectionPage', () => {
  let component: VoteselectionPage;
  let fixture: ComponentFixture<VoteselectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoteselectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteselectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
