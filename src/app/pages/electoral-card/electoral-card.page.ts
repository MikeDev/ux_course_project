import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { deepCopy } from '@firebase/util';

@Component({
  selector: 'app-electoral-card',
  templateUrl: './electoral-card.page.html',
  styleUrls: ['./electoral-card.page.scss'],
})
export class ElectoralCardPage implements OnInit {
  card_values = []
  constructor(private router: Router, private loc: Location) {
    let e = deepCopy(this.router.getCurrentNavigation().extras.state['elector'])
    delete e['password']
    delete e['key']
    e['birthday'] = new Date(e['birthday']).toLocaleDateString('en-GB')
    this.card_values = Object.entries(e)
   }

   goback(){
     this.loc.back()
   }

  ngOnInit() {
  }

}
