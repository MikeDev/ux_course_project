import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectoralCardPage } from './electoral-card.page';

describe('ElectoralCardPage', () => {
  let component: ElectoralCardPage;
  let fixture: ComponentFixture<ElectoralCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectoralCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectoralCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
