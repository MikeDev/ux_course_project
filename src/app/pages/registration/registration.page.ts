import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserManagerService } from 'src/app/service/user-manager.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  email: string = ""
  passport: string = ""
  password: string = ""

  constructor(private alert_ctrl: AlertController,
    private router: Router,
    public loc: Location,
    private user_manager: UserManagerService) {
  }

  ngOnInit() {
  }

  private send_data() {
    // Send data to firebase
  }

  private create_thank_message() {
    return this.alert_ctrl.create({
      header: 'Thank you for the registration',
      subHeader: 'In next days an human operator will validate your registration request. At the completion of the procedure you will receive an email',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.router.navigateByUrl('/login')
        }
      }]
    })
  }


  complete() {
    let userdata = {
      name: 'Name',
      surname: 'Surname',
      email: this.email,
      passport: this.passport,
      password: this.password
    }
    this.user_manager.new_user(userdata)
    let alert = this.create_thank_message().then(
      (a) => { a.present() }
    )
  }

}
