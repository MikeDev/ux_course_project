import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VotedisclamerPage } from './votedisclamer.page';

const routes: Routes = [
  {
    path: '',
    component: VotedisclamerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VotedisclamerPage]
})
export class VotedisclamerPageModule {}
