import { Component, OnInit } from '@angular/core';
import { Vote } from 'src/app/class/vote';
import { Router, NavigationExtras, Navigation } from '@angular/router';
import { Location } from '@angular/common';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-votedisclamer',
  templateUrl: './votedisclamer.page.html',
  styleUrls: ['./votedisclamer.page.scss'],
})
export class VotedisclamerPage implements OnInit {
  public vote: Vote
  constructor(private router: Router,
    public loc: Location,
    private alert_ctrl: AlertController) {
    this.vote = this.router.getCurrentNavigation().extras.state['vote']

  }

  ngOnInit() {
  }
  private create_warning_msg(handler) {
    return this.alert_ctrl.create({
      header: 'Be careful to be alone while voting',
      subHeader: '',
      buttons: [{
        text: 'Ok',
        handler: handler
      }]
    })
  }


  public goto_voteselection(vote: Vote) {
    let f = () => this.router.navigateByUrl('/chamberselection/deputies', { state: { 'vote': vote, chamber: 'deputies'} })
    let warning = this.create_warning_msg(f)
    warning.then((w) => w.present())
  }

}
