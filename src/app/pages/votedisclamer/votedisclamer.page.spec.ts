import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotedisclamerPage } from './votedisclamer.page';

describe('VotedisclamerPage', () => {
  let component: VotedisclamerPage;
  let fixture: ComponentFixture<VotedisclamerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotedisclamerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotedisclamerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
