import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/class/user';
import { PopoverController } from '@ionic/angular';
import { PopoverMenuComponent } from '../hamburgesa-menu/popover-menu.component';
import { VoteProviderService } from 'src/app/service/vote-provider.service';
import { UserManagerService } from 'src/app/service/user-manager.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss'],
})
export class HomepagePage implements OnInit {

  public user: User

  constructor(private router: Router,
              private popover_ctrl: PopoverController,
              private user_service: UserManagerService) {
    let has_state = this.router.getCurrentNavigation().extras.state !== undefined
    if(user_service.is_set_current_user()){
      this.user = user_service.get_current()
    } else {
      if(has_state && Object.keys(this.router.getCurrentNavigation().extras.state).includes('user')){
        this.user = this.router.getCurrentNavigation().extras.state['user']
        this.user_service.set_current(this.user)
      }
    }
  }

  public async present_popover(event){
    let popover = await this.popover_ctrl.create({
      component: PopoverMenuComponent,
      event: event,
      componentProps: {user: this.user}
    })
    return await popover.present()
  }

  ngOnInit() {
  }

}
