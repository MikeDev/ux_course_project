import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomepagePage } from './homepage.page';
import { PopoverMenuComponent } from '../hamburgesa-menu/popover-menu.component';

const routes: Routes = [
  {
    path: '',
    component: HomepagePage,
    children: [
      {
        path: 'actualvotes',
        children: [
          {
            path: '',
            loadChildren: '../votelist/votelist.module#VotelistPageModule',
            data: {'mode': 'actual'}
          }
        ]
      },
      {
        path: 'oldvotes',
        children: [
          {
            path: '', 
            loadChildren: '../votelist/votelist.module#VotelistPageModule',
            data: {'mode': 'old'}
          }
        ]
      },
      {
        path: 'futurevotes',
        children: [
          {
            path: '',
            loadChildren: '../votelist/votelist.module#VotelistPageModule',
            data: {'mode': 'future'}

          }
        ]
      },
      {
        path: '',
        redirectTo: '/homepage/actualvotes',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/homepage/actualvotes',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [PopoverMenuComponent],
  declarations: [HomepagePage, PopoverMenuComponent]
})
export class HomepagePageModule {}
