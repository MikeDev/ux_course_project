import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import {User} from '../class/user';


@Injectable({
  providedIn: 'root'
})
export class UserManagerService {

  private userdb = this.db.list<User>('User');
  private current: User = undefined

  constructor(private db: AngularFireDatabase) { }

  public new_user(user) {
    this.userdb.push(user)
  }

  public pull_users(): Observable<User[]> {
    return this.userdb
      .snapshotChanges()
      .map(
        (changes) => changes.map(
          (c) => {
            let values = c.payload.val()
            let user = Object.assign(new User(), values)
            user.key = c.key
            return user
          }
        )
      )
  }

  public set_current(user: User){
    this.current = user
  }

  public get_current(): User{
    return this.current
  }

  public eliminate_current(){
    this.current = undefined
  }

  public is_set_current_user(): boolean{
    return this.current !== undefined
  }
}

