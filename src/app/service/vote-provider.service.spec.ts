import { TestBed } from '@angular/core/testing';

import { VoteProviderService } from './vote-provider.service';

describe('VoteProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VoteProviderService = TestBed.get(VoteProviderService);
    expect(service).toBeTruthy();
  });
});
