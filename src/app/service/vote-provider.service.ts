import { Injectable } from '@angular/core';
import { Vote } from '../class/vote';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VoteProviderService {

  public votes: Array<Vote> = []
  // public votes: Array<Vote> = [
  //   new Vote(new Date(2012, 5, 4),
  //     'National election',
  //     'Election of president C',
  //     true, false, false, false,
  //     { results: [271294, 212321, 84781], choises: {
  //       'Partito A': ['Marco S.', 'Matteo B.', 'Sandro D.'],
  //       'Partito B': ['Paolo C.', 'Carlo E.', 'Marco T.'],
  //       'Partito C': ['Alessio N.', 'Tommaso L.', 'Riccardo O.']
  //     }, toselect: 2 }),
  //   new Vote(new Date(2018, 5, 4),
  //     'National election',
  //     'Election of president D',
  //     true, false, false, false,
  //     { results: [148823, 8321, 48322], choises: ["Aldo", "Giovanni", "Giacomo"] }),
  //   new Vote(new Date(2020, 8, 4),
  //     'National election',
  //     'Election of president B',
  //     false, true, false),
  //   new Vote(new Date(2019, 8, 4),
  //     'National election',
  //     'Election of president C',
  //     false, false, true, false, 
  //     { results: [271294, 212321, 84781], choises: {
  //       'Partito A': ['Marco S.', 'Matteo B.', 'Sandro D.'],
  //       'Partito B': ['Paolo C.', 'Carlo E.', 'Marco T.'],
  //       'Partito C': ['Alessio N.', 'Tommaso L.', 'Riccardo O.'] } 
  //     })
  // ]
  private current_date = new Date(2019, 8, 4)

  private votedb = this.db.list<Vote>('Vote');


  constructor(private db: AngularFireDatabase) {
  }

  public classify_vote(vote: Vote): Vote {
    vote.is_old = this.current_date > vote.date
    vote.is_future = vote.date > this.current_date
    vote.is_actual = !vote.is_old && !vote.is_future
    vote.voted = false
    return vote
  }

  public get_results_by_id(id: number): Array<Array<any>> {
    for (let v of this.votes) {
      if (v.id == id) {
        return v.results
      }
    }
    return null
  }

  public get_votes(): Array<Vote> {
    console.log(JSON.stringify(this.votes))
    return this.votes
  }

  public pull_store_votes() {
    this.votes = []
    this.pull_votes().subscribe((votes) => this.votes = this.votes.concat(votes))
  }

  public pull_votes(): Observable<Vote[]> {
    return this.votedb
      .snapshotChanges()
      .map(
        (changes) => changes.map(
          (c) => {
            let values = c.payload.val()
            let vote = Object.assign(new Vote(null, null, null), values)
            if (Object.keys(vote).includes('date')) {
              vote['date'] = new Date(vote['date'])
            }
            vote.key = c.key
            vote = this.classify_vote(vote)
            return vote
          }
        )
      )
  }

  public update_vote(v: Vote) {
    if (v.key != null) {
      this.votedb.update(v.key, v)
    }
  }
}
