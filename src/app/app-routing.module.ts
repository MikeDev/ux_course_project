import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'registration', loadChildren: './pages/registration/registration.module#RegistrationPageModule' },
  { path: 'homepage', loadChildren: './pages/homepage/homepage.module#HomepagePageModule' },
  { path: 'votedisclamer', loadChildren: './pages/votedisclamer/votedisclamer.module#VotedisclamerPageModule' },
  { path: 'voteselection', loadChildren: './pages/voteselection/voteselection.module#VoteselectionPageModule' },
  { path: 'voteresults', loadChildren: './pages/voteresults/voteresults.module#VoteresultsPageModule' },
  { path: 'votelist', loadChildren: './pages/votelist/votelist.module#VotelistPageModule' },
  { path: 'candidatesselection', loadChildren: './pages/candidatesselection/candidatesselection.module#CandidatesselectionPageModule' },
  { path: 'electoral-card', loadChildren: './pages/electoral-card/electoral-card.module#ElectoralCardPageModule' },
  { path: 'chamberselection', loadChildren: './pages/chamberselection/chamberselection.module#ChamberselectionPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
